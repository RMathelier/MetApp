var express = require('express');
var router = express.Router();

//Instanciation d'influxDB
const Influx = require('influx');
const influx = new Influx.InfluxDB({
 host: 'localhost',
 port: 8086,
 database: 'metappdb',
 schema: [
   {
     measurement: 'location',
     fields: {
       longitude: Influx.FieldType.STRING,
       latitude: Influx.FieldType.STRING
     },
     tags: []
   },
   {
     measurement: 'rainfall',
     fields: {
       value: Influx.FieldType.STRING
     },
     tags: []
   },
   {
     measurement: 'sensor',
     fields: {
       temperature: Influx.FieldType.STRING,
       pressure: Influx.FieldType.STRING,
       humidity: Influx.FieldType.STRING,
       luminosity: Influx.FieldType.STRING,
       wind_heading: Influx.FieldType.STRING,
       wind_speed_avg: Influx.FieldType.STRING,
       wind_speed_max: Influx.FieldType.STRING,
       wind_speed_min: Influx.FieldType.STRING
     },
     tags: []
   }
 ]
})

//Construction de la requête à la BDD
function buildQuery(table, attribute, isLast, start, stop){

  if(table == "measurements"){ table = "measures"; }

  query = "SELECT " + attribute + " FROM " + table + " ";
  if(isLast){
    query += "ORDER BY time desc LIMIT 1";
  }
  else{
    query += " WHERE time >= '" + start + "' AND time <= '" + stop + "' ORDER BY time desc";
  }
  console.log(query);
  return query;
}

//Construction de la promesse
function buildPromise(table, attribute, isLast, start = "", stop = ""){
  let promise = influx.query(buildQuery(table, attribute, isLast, start, stop))
  .then(result => {
    result.forEach(function (e) {
      e.date = e.time;
      delete e.time;
      if(table == "rainfall"){ delete e.value; }
    });
    return result;
  })
  .catch(err => {
    //res.status(500).send(err.stack);
  })
  return promise;
}

// ****** ROUTEURS *******

router.get('/', function(req, res, next){
  res.send("Bienvenue dans l'API MetApp.");
})

// *** API 'LAST' ***

router.get('/last', function(req, res, next){
  //Récupère toutes l'ensemble des dernières données.

  let attribute = "*";

  let promiseLocation = buildPromise("location", attribute, true);
  let promiseRainfall = buildPromise("rainfall", attribute, true);
  let promiseMeasurements = buildPromise("measurements", attribute, true);

  Promise.all([promiseLocation, promiseRainfall, promiseMeasurements]).then(function(values) {

    //Construction du json
    let resultText = '{"location" : ' + JSON.stringify(values[0]) + ', "measurements" : ' + JSON.stringify(values[2]) + ', "rainfall" : ' + JSON.stringify(values[1]) + '}';
    let resultJSON = JSON.parse(resultText);
    res.json(resultJSON);

  });

});

router.get('/last/:param/', function(req, res, next){
  //Récupère la dernière donnée pour une des 3 tables (location, rainfall ou measurements).

  let param = req.params.param;
  let attribute = "*";

  if( param == "location" || param == "rainfall" || param == "measurements"){
    buildPromise(param, attribute, true)
    .then(result => { res.json(result); });
  }
  else{
    res.status(404).send("Bad request : the parameter must be location, measurements or rainfall.");
  }
});

router.get('/last/:param/:attribute', function(req, res, next){
  //Récupère la dernière valeur d'un attribut.

  let param = req.params.param;
  let attribute = req.params.attribute;

  if( param == "location" || param == "rainfall" || param == "measurements"){
    buildPromise(param, attribute, true)
    .then(result => { res.json(result); });
  }
  else{
    res.status(404).send("Bad request : the parameter must be location, measurement or rainfall.");
  }
});


// *** API 'INTERVAL' ***

router.get('/interval', function(req, res, next){
  //Récupère toutes les données pour un intervalle de temps donné.

  let attribute = "*";

  let start = req.query.start;
  let stop = req.query.stop;

  let promiseLocation = buildPromise("location", attribute, false, start, stop);
  let promiseRainfall = buildPromise("rainfall", attribute, false, start, stop);
  let promiseMeasurements = buildPromise("measurements", attribute, false, start, stop);

  Promise.all([promiseLocation, promiseRainfall, promiseMeasurements]).then(function(values) {

    //Construction du json
    let resultText = '{"location" : ' + JSON.stringify(values[0]) + ', "measurements" : ' + JSON.stringify(values[2]) + ', "rainfall" : ' + JSON.stringify(values[1]) + '}';
    let resultJSON = JSON.parse(resultText);
    res.json(resultJSON);

  });

})

router.get('/interval/:param/', function(req, res, next){
  //Récupère toutes les données d'une des 3 tables pour un intervalle de temps donné.

  let param = req.params.param;
  let attribute = "*";

  let start = req.query.start;
  let stop = req.query.stop;

  buildPromise(param, attribute, false, start, stop)
  .then(result => { res.json(result); });

});

router.get('/interval/:param/:attribute/', function(req, res, next){
  //Récupère toutes les valeurs d'un attribut pour un intervalle de temps donné.

  let param = req.params.param;
  let attribute = req.params.attribute;

  let start = req.query.start;
  let stop = req.query.stop;

  buildPromise(param, attribute, false, start, stop)
  .then(result => { res.json(result); });

});

module.exports = router;
