const http = require('http');
const fs = require('fs');

//Instanciation d'influxDB
const Influx = require('influx');
const influx = new Influx.InfluxDB({
 host: 'localhost:8086',
 database: 'metappdb',
 schema: [
   {
     measurement: 'location',
     fields: {
       longitude: Influx.FieldType.STRING,
       latitude: Influx.FieldType.STRING
     },
     tags: []
   },
   {
     measurement: 'rainfall',
     fields: {
       value: Influx.FieldType.STRING
     },
     tags: []
   },
   {
     measurement: 'measures',
     fields: {
       temperature: Influx.FieldType.STRING,
       pressure: Influx.FieldType.STRING,
       humidity: Influx.FieldType.STRING,
       luminosity: Influx.FieldType.STRING,
       wind_heading: Influx.FieldType.STRING,
       wind_speed_avg: Influx.FieldType.STRING,
       wind_speed_max: Influx.FieldType.STRING,
       wind_speed_min: Influx.FieldType.STRING
     },
     tags: []
   }
 ]
})

//Real work
timeout();

function timeout(){

  setTimeout(() => {

    //Récupère les données du capteur
    fs.readFile("/dev/shm/gpsNmea", 'utf8', (err, data) => {
      if(data != ""){
        //Transformation des données GPS brutes
        let GPSarray = Nmea2Array(data);
        console.log(GPSarray[0]);
        console.log(typeof(GPSarray[0]));
        //On récupère seulement les données RMC
        if(GPSarray[0] !== 0){
          //Ajoute les données dans la base
          influx.writeMeasurement('location',[
            {
              fields: { longitude: GPSarray[2],
                        latitude: GPSarray[1]
                      },
              timestamp: new Date(GPSarray[0]),
            }
          ])
        }
      }
      else{
        console.log("ligne vide");
      }
    });
    timeout();

  }, 500);

}


function minutesToDeg(dm){
    var Deg = 0;
    var min='';

    if(dm.split('.')[0].length===4){
        for(var i = 2; i<dm.length ; i++){
            min += dm[i];
        }
        min /= 60;
        Deg = parseInt(dm[0]+dm[1])+min;
    }
    else if (dm.split('.')[0].length===5){
        for(var i = 3; i<dm.length ; i++){
            min += dm[i];
        }
        min /= 60;
        Deg = parseInt(dm[0]+dm[1]+dm[2])+min;
    }
    else{
        console.log("erreur length "+dm.split('.')[0].length);
    }
    return Deg;
}

function Nmea2Array(data){

  const arrRMC = data.split(',');
  var arrFinRMC;

  if(arrRMC[16]==='A'){

    // GESTION CARDINALS
    var ns = 1;
    var ew = 1;
    if(arrRMC[18]==='S'){ns=-1};
    if(arrRMC[20]==='W'){ew=-1};

    //GESTION UTC
    var secondes = '';
    for(var i = 4; i<arrRMC[15].length ; i++){
        secondes += arrRMC[15][i];
    }
    const hUTC = arrRMC[15][0]+arrRMC[15][1]+":"+arrRMC[15][2]+arrRMC[15][3]+":"+secondes;
    const jUTC = '20'+arrRMC[23][4]+arrRMC[23][5]+"-"+arrRMC[23][2]+arrRMC[23][3]+"-"+arrRMC[23][0]+arrRMC[23][1];
    const UTC = jUTC+'T'+hUTC+'Z';

    // GESTION TRANSFO DEGRE
    const latDeg = minutesToDeg(arrRMC[17]);
    const lonDeg = minutesToDeg(arrRMC[19]);

    //ENREGISTREMENT FINAL
    arrFinRMC = [UTC,ns*latDeg,ew*lonDeg];
  }

  else{
    arrFinRMC = [0,0,0];
  }

  return arrFinRMC;
}
