const http = require('http');
const fs = require('fs');


//Instanciation d'influxDB
const Influx = require('influx');
const influx = new Influx.InfluxDB({
 host: 'localhost:8086',
 database: 'metappdb',
 schema: [
   {
     measurement: 'location',
     fields: {
       longitude: Influx.FieldType.STRING,
       latitude: Influx.FieldType.STRING
     },
     tags: []
   },
   {
     measurement: 'rainfall',
     fields: {
       value: Influx.FieldType.STRING
     },
     tags: []
   },
   {
     measurement: 'measures',
     fields: {
       temperature: Influx.FieldType.STRING,
       pressure: Influx.FieldType.STRING,
       humidity: Influx.FieldType.STRING,
       luminosity: Influx.FieldType.STRING,
       wind_heading: Influx.FieldType.STRING,
       wind_speed_avg: Influx.FieldType.STRING,
       wind_speed_max: Influx.FieldType.STRING,
       wind_speed_min: Influx.FieldType.STRING
     },
     tags: []
   }
 ]
})

//Real work
timeout();

function timeout(){

  setTimeout(() => {

    //Récupère les données du capteur
    fs.readFile("/dev/shm/rainCounter.log", 'utf8', (err, data) => {
      let date = new Date(data.trim());
      if(data != ""){
        //Ajoute les données dans la base
        influx.writeMeasurement('rainfall',[
          {
            fields: { value: "0"
                    },
            timestamp: date,
          }
        ])
      }
      else{
        console.log("ligne vide");
      }
    });
    timeout();

  }, 500);


}
