# SONDE Application Météo

La sonde météo est un raspberry qui génère des données météo.   
On trouve sur celle-ci un serveur qui enregistre les données météo dans une base de données et qui fournit des API de lecture de cette base de données. 
La sonde doit être accessible quelque soit le navigateur qui l'interroge. Le format d'échange est donc standardisé.   

Le projet a donc pour objectif de créer un programme qui lit les données, les stocke et les expose grâce à un service Web. 

[Dépôt contenant la source de la centrale](https://gitlab.com/JNourry/MetApp)

![alt text](logoSonde.png)

## Table of Contents

1. [Installation](#installation)
2. [Lancement de la sonde](#lancement)
3. [API proposées](#api)
4. [Améliorations possibles](#ameliorations) 

## Installation (sur Ubuntu)

* [Node.js](https://nodejs.org/en/download/)

* [Express](http://expressjs.com/fr/starter/installing.html) 

### Coté base de données

* [InfluxDB](https://docs.influxdata.com/influxdb/v1.4/introduction/installation/)

Installer influxdb :
```
sudo apt-get update && sudo apt-get install influxdb
```
Démarrer influxdb (on utilise systemctl sur la nouvelle version d'influxdb) :
```
sudo systemctl start influxdb
```
Vérifier le statut (doit être à "enabled") :
```
sudo systemctl status influxdb
```
Installation de la commande influx, présente dans un package séparé pour les version plus récentes :
```
sudo apt-get install influxdb-client
```
Lancement d'influx pour effectuer des requêtes
```
influx -precision rfc3339
```


**[Back to top](#table-of-contents)** 

## Lancement de la sonde

Connexion en SSH sur le raspberry :
```
ssh pi@(ip du raspberry)
```

### Coté base de données
La fakeSonde présente sur le Raspberry émet en continu des données météo factices. La première étape consiste à récupérer ses données pour les enregistrer dans la base de données.
La fakeSonde génère 3 fichiers : 
* gpsNmea : données GPS.
* rainCounter.log : données du rainfall.
* sensor : diverses données météo (température, humidité, luminosité, vent...)

On va donc créer une table pour chaque fichier, à l'aide de 3 scripts, à lancer indépendemment.
On se déplace tout d'abord dans le dossier contenant ces scripts :
```
cd ~/MetApp
```
Lancement des 3 scripts :
```
node Sensor2DB.js
```
```
node Rainfall2DB.js
```
```
node GPS2DB.js
```

### Coté serveur
Le Raspberry héberge également l'API qui va diffuser les données en temps réel dans le format JSON standardisé.
Pour lancer le serveur, on se place dans le dossier qui le contient :
```
cd ~/MetApp/metapi
```
Lancement du serveur :
```
npm start
```

**[Back to top](#table-of-contents)**

## API proposées 
Pour accéder à l'API depuis le client, se connecter à l'IP suivant : 172.31.43.58, port 3000.
2 API sont disponibles :
* API Last : fournit les dernières données en date.
* API Interval : fournit les données dans certain un intervalle de temps.

On accède au données via les requêtes suivantes
```
GET IP/last/(1)/(2)
```
```
GET IP/interval/(1)/(2)?start=(3)&stop=(3)
```
(1) : la table souhaitée (gps, measurements ou rainfall, correspondant aux trois fichiers de la fakeSonde).

(2) : l'attribut souhaité (par exemple longitude pour gps, ou temperature pour measurements).
Si ces champs ne sont pas renseignés, on récupère l'ensemble des données.

(3) : les dates de début et de fin, en format timestamp valide : YYY-MM-DDThh:mm:ss.sssZ

**[Back to top](#table-of-contents)**

## Format des données

```json
{"location":[{"latitude":"48.117466666666665","longitude":"-1.5219833333333335","date":"2018-01-24T12:47:46.110Z"}],"measurements":[{"humidity":"50.0","luminosity":"4974","pressure":"995.02","temperature":"9.99","wind_heading":"189.870345792188","wind_speed_avg":"39.8","wind_speed_max":"64.0","wind_speed_min":"25.4","date":"2018-01-24T12:47:46.114Z"}],"rainfall":[{"date":"2018-01-24T12:47:44.707Z"}]}
```

**[Back to top](#table-of-contents)**

## Authors

* **Rose Mathelier**
* **Justine Nourry**
* **Laure Le Breton**

**[Back to top](#table-of-contents)**

