const http = require('http');
const fs = require('fs');

//Instanciation d'influxDB
const Influx = require('influx');
const influx = new Influx.InfluxDB({
 host: 'localhost',
 port: 8086,
 database: 'metappdb',
 schema: [
   {
     measurement: 'location',
     fields: {
       longitude: Influx.FieldType.STRING,
       latitude: Influx.FieldType.STRING
     },
     tags: []
   },
   {
     measurement: 'rainfall',
     fields: {
       value: Influx.FieldType.STRING
     },
     tags: []
   },
   {
     measurement: 'measures',
     fields: {
       temperature: Influx.FieldType.STRING,
       pressure: Influx.FieldType.STRING,
       humidity: Influx.FieldType.STRING,
       luminosity: Influx.FieldType.STRING,
       wind_heading: Influx.FieldType.STRING,
       wind_speed_avg: Influx.FieldType.STRING,
       wind_speed_max: Influx.FieldType.STRING,
       wind_speed_min: Influx.FieldType.STRING
     },
     tags: []
   }
 ]
})

//Real work
timeout();

function timeout(){

  setTimeout(() => {

    //Récupère les données du capteur
    fs.readFile("/dev/shm/sensors", 'utf8', (err, data) => {
      if(data != ""){
        let json = JSON.parse(data);
        let date = new Date(json.date.trim());
        //Ajoute les données dans la base
        influx.writeMeasurement('measures',[
          {
            fields: { temperature: json.measure[0].value,
                      pressure: json.measure[1].value,
                      humidity: json.measure[2].value,
                      luminosity: json.measure[3].value,
                      wind_heading: json.measure[4].value,
                      wind_speed_avg: json.measure[5].value,
                      wind_speed_max: json.measure[6].value,
                      wind_speed_min: json.measure[7].value
                    },
            timestamp: date,
          }
        ])
      }
      else{
        console.log("ligne vide");
      }
    });
    timeout();

  }, 500);

}
